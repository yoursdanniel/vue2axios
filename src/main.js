// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// import axios from "axios"; //直接在组建中引用url

//请求方法
// import {axiosGet,axiosPost,axiosDelete} from "@/serversApi/request.js"
// Vue.prototype.$axiosGet = axiosGet;
// Vue.prototype.$axiosPost = axiosPost;
// Vue.prototype.$axiosDelete = axiosDelete;//直接在组建中引用url

Vue.config.productionTip = false
// Vue.prototype.$axios = axios



/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
